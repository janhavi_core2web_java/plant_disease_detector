package com.plant.controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;


import java.util.HashMap;
import java.util.Map;

import com.plant.firebaseConfig.DataService;

public class SignUpController {
private LoginController loginController;


    public SignUpController(LoginController loginController){

        this.loginController = loginController;


    }
    
    public Scene createSignupScene(Stage prStage){

        Label lb = null;

        Label userLabel = new Label("Username:");
         userLabel.setTextFill(Color.BLACK);
        userLabel.setFont(new Font(36));
        userLabel.setStyle("-fx-font-weight: bold;");

        TextField userTextField = new TextField();
        userTextField.setStyle("-fx-set-pref-width:350");


        Label passLabel = new Label("Password");
        passLabel.setTextFill(Color.BLACK);
        passLabel.setFont(new Font(36));
        passLabel.setStyle("-fx-font-weight: bold;");


        PasswordField passField = new PasswordField();
        passField.setStyle("-fx-set-pref-width:350");

        Button signupButton = new Button("SignUp");
        signupButton.setStyle("-fx-border-color:black;-fx-border-width:2px;-fx-border-radius:5px;");
        signupButton.setFont(new Font(26));
        signupButton.setStyle("-fx-font-weight: bold;");
        signupButton.setMinSize(180, 50);
        signupButton.setStyle("-fx-background-color:AQuA"); 

        lb = new Label("SIGNUP HERE !");
        lb.setTextFill(Color.LIMEGREEN);
        lb.setStyle("-fx-font-weight: bold;");
        lb.setFont(new Font(44));
        


        VBox fieldBox1 = new VBox(10,userLabel,userTextField);
        VBox fieldBox2 = new VBox(10,passLabel,passField);

        fieldBox1.setMaxSize(300,30 );
        fieldBox2.setMaxSize(300, 30);
        
        HBox buttonBox = new HBox(50,signupButton);

        buttonBox.setMaxSize(350, 30);
        buttonBox.setAlignment(Pos.CENTER);

        signupButton.setOnAction(new EventHandler<ActionEvent>() {
            

            @Override
            public void handle(ActionEvent event){
            
                handleSignup(prStage, userTextField.getText() ,passField.getText());


            }
        });
    
            VBox signBox = new VBox(20,lb,fieldBox1,fieldBox2,buttonBox);
            signBox.setAlignment(Pos.CENTER);
            signBox.setMaxSize(500, 500);
            signBox.setStyle("-fx-border-color: white; -fx-border-width: 2px; -fx-border-radius: 10px; -fx-padding: 20px; -fx-background-color: rgba(255, 255, 255, 0.8); -fx-background-radius: 10px;");

            
            VBox vBox = new VBox(30,signBox);
            vBox.setAlignment(Pos.CENTER);
            vBox.setStyle("-fx-background-image: url('" + getClass().getResource("/bg3.jpg").toExternalForm() + "'); -fx-background-size: cover;");
           
            return new Scene(vBox,600,600);

           
            }

    private void handleSignup(Stage prStage,String username, String password){
       
       
        DataService dataService;
        try{   

            dataService = new DataService();
             
            Map<String, Object> data = new HashMap<>();

            data.put("password",password);
            data.put("username",username);

            System.out.println(username);
            System.out.println(password);

            dataService.addData("users",username, data);
            System.out.println("User Registered successfully");
         

        }
        catch(Exception e){
            e.printStackTrace();
            
        }

        loginController.showLoginScene();
    }
}