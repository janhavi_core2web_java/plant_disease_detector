package com.plant.controller;
import java.util.concurrent.ExecutionException;

import com.plant.Dashboard.UserPage;
import com.plant.firebaseConfig.DataService;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class LoginController extends HBox {
    private Stage prStage;

    private Scene loginScene;

    private Scene userScene;

    private DataService dataService;

    public static String key;

    public LoginController(Stage prStage){

        this.prStage = prStage;
        dataService = new DataService();
        initScenes();
        
    }

    private void initScenes(){
        initLoginScene();
    }

    private void initLoginScene(){
       
        Image ig2 = new Image(getClass().getResource("/pLogo.png").toExternalForm());

        ImageView iv2 = new ImageView(ig2);
        iv2.setFitHeight(300);
        iv2.setFitWidth(300);

        Circle clip = new Circle(150, 150, 150);
        iv2.setClip(clip);
       
        Label lb = new Label("Welcome to Plant Care");
        lb.setTextFill(Color.BEIGE);
        lb.setStyle("-fx-font-weight: bold;");
        lb.setFont(new Font(44));

        Label userLabel = new Label("Username");
        userLabel.setTextFill(Color.BLACK);
        userLabel.setFont(new Font(36));
        userLabel.setStyle("-fx-font-weight: bold;");

        TextField userTextField = new TextField();
        userTextField.setStyle("-fx-set-pref-width:350");

        Label passLabel = new Label("Password");
        passLabel.setTextFill(Color.BLACK);
        passLabel.setFont(new Font(36));
        passLabel.setStyle("-fx-font-weight: bold;");

        PasswordField passField = new PasswordField();
        passField.setStyle("-fx-set-pref-width:350");

        Button loginButton = new Button("Login");
        loginButton.setStyle("-fx-border-color:black;-fx-border-width:2px;-fx-border-radius:5px;");
        //loginButton.setTextFill(Color.BLACK);
        loginButton.setFont(new Font(26));
        loginButton.setTextFill(Color.WHITE);
        loginButton.setStyle("-fx-font-weight: bold;");
        loginButton.setMinSize(180, 20);
        //loginButton.setStyle("-fx-background-color:#004953");
        loginButton.setStyle("-fx-background-color:#004953; -fx-text-fill: white; -fx-font-size: 21px; -fx-padding: 10px 20px;-fx-background-radius: 40;");
        

        Button signupButton = new Button("SignUp");
        signupButton.setStyle("-fx-border-color:Black;-fx-border-width:2px;-fx-border-radius:5px;");
        signupButton.setTextFill(Color.WHITE);
        signupButton.setFont(new Font(26));
        signupButton.setStyle("-fx-font-weight: bold;");
        signupButton.setMinSize(180, 40);
        signupButton.setStyle("-fx-background-color:#004953; -fx-text-fill: white; -fx-font-size: 21px; -fx-padding: 10px 20px;-fx-background-radius: 40;");

        

        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event){

                handleLogin(userTextField.getText(),passField.getText());

                userTextField.clear();
                passField.clear();
            }
        });

        signupButton.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event){

                showSignupScene();
                

                userTextField.clear();
                passField.clear();
            }
        });

        VBox fieldBox1 = new VBox(10,userLabel,userTextField);
        fieldBox1.setMaxSize(350, 50);
        
        VBox fieldBox2 = new VBox(10,passLabel,passField);
        fieldBox2.setMaxSize(350, 50);

        HBox buttonBox = new HBox(30,loginButton,signupButton);
        buttonBox.setMaxSize(350, 50);
        buttonBox.setAlignment(Pos.CENTER);

        VBox vb1 = new VBox(30,iv2,lb);
        vb1.setAlignment(Pos.CENTER);
        vb1.setMaxSize(200, 80);
        vb1.setLayoutX(400);
        vb1.setLayoutY(100);

        VBox vb2 = new VBox(30,fieldBox1,fieldBox2,buttonBox);
        vb2.setAlignment(Pos.CENTER);
        vb2.setMaxSize(200, 400);
        vb2.setLayoutX(800);
        vb2.setLayoutY(150);

        VBox loginForm = new VBox(30, fieldBox1, fieldBox2, buttonBox);
        loginForm.setAlignment(Pos.CENTER);
        loginForm.setMaxSize(400, 400);
        loginForm.setStyle("-fx-border-color: white; -fx-border-width: 2px; -fx-border-radius: 10px; -fx-padding: 20px; -fx-background-color: rgba(255, 255, 255, 0.8); -fx-background-radius: 10px;");

        VBox vBox = new VBox(30, iv2, lb, loginForm);
        vBox.setAlignment(Pos.CENTER);
        vBox.setStyle("-fx-background-image: url('" + getClass().getResource("/bg2.jpg").toExternalForm() + "'); -fx-background-size: cover;");
        
        loginScene = new Scene(vBox,600,600);
        loginScene.setCursor(Cursor.DEFAULT);
        
       
    }
    private void initUserScene(){

        UserPage userPage = new UserPage(dataService);
        userScene = new Scene(userPage.createUserScene(this::handleLogout),
        600,600);

    }

    public Scene getLoginScene(){

        return loginScene;
    }

    public void showLoginScene(){

        prStage.setScene(loginScene);
        prStage.setTitle("Login");
        prStage.show();
    }
    
    private void handleLogin(String username, String password){

        try{
            
            if(dataService.authenticateUser(username, password)){

                key = username;
                initUserScene();

                prStage.setScene(userScene);
                prStage.setTitle("User DashBoard");

                }
                else{

                    System.out.println("Invalid credentials");
                    key = null;
                }
        }
        catch(ExecutionException | InterruptedException ex){

            ex.printStackTrace();
        }

    }
    private void showSignupScene(){

        SignUpController signUpController = new SignUpController(this);

        Scene signupScene = signUpController.createSignupScene(prStage);

        prStage.setScene(signupScene);
        prStage.setTitle("SignUp");
        prStage.show();

    }

    private void handleLogout(){
        prStage.setScene(loginScene);
        prStage.setTitle("Login");
    }

    
}