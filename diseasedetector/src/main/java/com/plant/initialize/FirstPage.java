package com.plant.initialize;

import com.plant.controller.LoginController;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class FirstPage extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Tagline Text
        Text tagline = new Text("WELCOME TO PLANT CARE");
        tagline.setStyle("-fx-font-size: 40px; -fx-font-weight: bold; -fx-fill: #ff4500;");
        tagline.setWrappingWidth(600);
        tagline.setTextAlignment(TextAlignment.CENTER);

        // Main Heading Text
        Text mainHeading = new Text("Effortlessly Care For your Plants!!");
        mainHeading.setStyle("-fx-font-size: 50px; -fx-font-weight: bold;");
        mainHeading.setWrappingWidth(600);
        mainHeading.setTextAlignment(TextAlignment.CENTER);

        // Sub Heading Text
        Text subHeading = new Text("Take care of your plants easily with us");
        subHeading.setStyle("-fx-font-size: 30px;");
        subHeading.setWrappingWidth(600);
        subHeading.setTextAlignment(TextAlignment.CENTER);

        // Services Button
        Button servicesButton = new Button("GET STARTED");
        servicesButton.setStyle("-fx-background-color: #0000ff; -fx-text-fill: white; -fx-font-size: 25px; -fx-padding: 10px 20px;-fx-background-radius: 40;");
        servicesButton.setAlignment(Pos.CENTER);
       
        servicesButton.setOnAction(new EventHandler<ActionEvent>() {
            LoginController loginController = new LoginController(primaryStage);
            @Override
            public void handle(ActionEvent event){
                System.out.println("Button clicked");
                primaryStage.setScene(loginController.getLoginScene());
                primaryStage.setTitle("User");
                //primaryStage.setFullScreen(true);
                primaryStage.setHeight(980);
                primaryStage.setWidth(1950);
                primaryStage.setResizable(true);
                primaryStage.getIcons().add(new Image(getClass().getResource("/pLogo.png").toExternalForm()));
            }
        });

        VBox maincontainer = new VBox(tagline, mainHeading, subHeading, servicesButton );
        maincontainer.setMaxSize(700,1000 );
        maincontainer.setAlignment(Pos.CENTER);
        maincontainer.setSpacing(20);


        // VBox Layout
        VBox vb = new VBox(maincontainer);
        vb.setAlignment(Pos.CENTER_RIGHT);

        Image backgroundImage = new Image(getClass().getResourceAsStream("/background.jpg")); // Update the path as necessary
        BackgroundImage bgImage = new BackgroundImage(
            backgroundImage,
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.CENTER,
            new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true)
        );

        Region backgroundRegion = new Region();
        backgroundRegion.setBackground(new Background(bgImage));
        backgroundRegion.setPrefSize(1900, 1000);

        Circle clip = new Circle(200,800,1000); 
        backgroundRegion.setClip(clip);

        StackPane stackPane = new StackPane(backgroundRegion, vb);
        StackPane.setAlignment(backgroundRegion, Pos.CENTER_LEFT);
        StackPane.setAlignment(vb, Pos.CENTER_RIGHT); // Align VBox to the right
        stackPane.setPadding(new Insets(0, 50, 0, 0));

        // Scene and Stage setup
        Scene scene = new Scene(stackPane, 1950, 980);
        primaryStage.setTitle("Plant Care");
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image(getClass().getResource("/pLogo.png").toExternalForm()));
        primaryStage.setResizable(true);
        primaryStage.show();
    }

}
