package com.plant;

import java.io.IOException;
import com.plant.initialize.FirstPage;
import com.plant.service.APIService;
import javafx.application.Application;

public class Main{
    public static void main(String[] args) 
    {
        System.out.println("Hello world!");
        Application.launch(FirstPage.class,args);
        Thread apiThread = new Thread(() -> {
            try {
                APIService.fetchData();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        apiThread.start();
  
    }
       
}
