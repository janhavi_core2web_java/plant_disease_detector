package com.plant.service;

import java.util.List;

public class Disease {
    public int id;
    public String common_name;
    public String scientific_name;
    public List<Description> description;
    public List<Image> images;
    public List<Solution> solution;

    public class Description {
        public String subtitle;
        public String description;
    }

    public class Solution {
        public String subtitle;
        public String description;
    }

    public class Image {
        //public String original_url;
        //public String regular_url;
        public String medium_url;
        //public String small_url;
        //public String thumbnail;
    }
}
