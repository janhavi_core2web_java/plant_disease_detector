package com.plant.service;

import java.io.IOException;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.lang.reflect.Type;

public class APIService {
    static List<Disease> diseases;

    public static void fetchData() throws IOException {
        StringBuffer response = new DataUrls().getresponseData();

        if (response != null) {
            try {
                JSONObject obj = new JSONObject(response.toString());

                if (obj.has("data")) {
                    JSONArray dataArray = obj.getJSONArray("data");
                    Type listType = new TypeToken<List<Disease>>() {}.getType();
                    diseases = new Gson().fromJson(dataArray.toString(), listType);
                } else {
                    System.out.println("Error: 'data' key not found in JSON response");
                }

            } catch (JSONException e) {
                System.out.println("Error parsing JSON response: " + e.getMessage());
                e.printStackTrace();
            }
        } else {
            System.out.println("Response is empty");
        }
    }

    public void displayDisease(VBox apiBox, int id) throws IOException {
        fetchData();

        apiBox.setMaxSize(500, 500);
        apiBox.setLayoutX(550);
        apiBox.setLayoutY(10);
        apiBox.setAlignment(Pos.TOP_LEFT);
        apiBox.setStyle("-fx-background-colur:Green;");

        if (diseases != null && !diseases.isEmpty()) {
            apiBox.getChildren().clear(); // Clear previous content

            boolean diseasefound = false;

            for (Disease disease : diseases) {
                if (disease.id == id) {
                    diseasefound = true;
                    Text tittlText = new Text(disease.common_name);
                    tittlText.setTextAlignment(TextAlignment.CENTER);
                    tittlText.setFont(new Font(40));
                    
                    Text commonName = new Text("Common Name: " + disease.common_name);
                    commonName.setTextAlignment(TextAlignment.LEFT);
                    commonName.setFont(new Font(30));

                    Text scientificName = new Text("Scientific Name: " + disease.scientific_name);
                    scientificName.setTextAlignment(TextAlignment.LEFT);
                    scientificName.setFont(new Font(30));

                    apiBox.getChildren().addAll(tittlText,commonName, scientificName);

                    for (Disease.Description description : disease.description) {
                        Text subtitle = new Text("Subtitle: " + description.subtitle);
                        subtitle.setTextAlignment(TextAlignment.LEFT);
                        subtitle.setFont(new Font(30));

                        Text desc = new Text("Description: " + description.description);
                        desc.setTextAlignment(TextAlignment.LEFT);
                        desc.setFont(new Font(30));
                        desc.setWrappingWidth(1600);

                        apiBox.getChildren().addAll(subtitle, desc);
                    }

                    for (Disease.Image image : disease.images) {
                        System.out.println("Loading image from URL: " + image.medium_url);

                        ImageView imageView = new ImageView(new Image(image.medium_url));

                        apiBox.getChildren().add(imageView);
                        break; // Display only the first image for simplicity
                    }
                }
            }

            if (!diseasefound) {
                System.out.println("No disease found with ID: " + id);
                Text errorText = new Text("No disease found with the specified ID.");
                apiBox.getChildren().add(errorText);
            }
        } else {
            System.out.println("Error: No data to display");
            Text errorText = new Text("No data available to display.");
            apiBox.getChildren().add(errorText);
        }
    }

    public void displaySolution(VBox apiBox, int id) throws IOException {
        fetchData();

        apiBox.setMaxSize(500, 500);
        apiBox.setLayoutX(550);
        apiBox.setLayoutY(10);
        apiBox.setAlignment(Pos.TOP_LEFT);

        if (diseases != null && !diseases.isEmpty()) {
            apiBox.getChildren().clear(); // Clear previous content

            boolean diseasefound = false;

            for (Disease disease : diseases) {
                if (disease.id == id) {
                    diseasefound = true;

                    Text solutionsTitle = new Text("Solutions for " + disease.common_name);
                    solutionsTitle.setTextAlignment(TextAlignment.CENTER);
                    solutionsTitle.setFont(new Font(40));
                    apiBox.getChildren().add(solutionsTitle);

                    for (Disease.Solution solution : disease.solution) {
                        Text subtitle = new Text("Subtitle: " + solution.subtitle);
                        subtitle.setFont(new Font(30));
                        subtitle.setTextAlignment(TextAlignment.LEFT);

                        Text desc = new Text("Description: " + solution.description);
                        desc.setFont(new Font(30));
                        desc.setTextAlignment(TextAlignment.LEFT);
                        desc.setWrappingWidth(1600);

                        apiBox.getChildren().addAll(subtitle, desc);
                    }
                    break;
                }
            }

            if (!diseasefound) {
                System.out.println("No disease found with ID: " + id);
                Text errorText = new Text("No disease found with the specified ID.");
                apiBox.getChildren().add(errorText);
            }
        } else {
            System.out.println("Error: No data to display");
            Text errorText = new Text("No data available to display.");
            apiBox.getChildren().add(errorText);
        }
    }
}
