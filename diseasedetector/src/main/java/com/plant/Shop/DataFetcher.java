package com.plant.Shop;


import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class DataFetcher {
    public static List<PlantMedicine> fetchData(String filePath) throws IOException {
        List<PlantMedicine> medicines = new ArrayList<>();
        try (InputStreamReader reader = new InputStreamReader(DataFetcher.class.getResourceAsStream(filePath))) {
            JSONTokener tokener = new JSONTokener(reader);
            JSONArray jsonArray = new JSONArray(tokener);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                PlantMedicine medicine = new PlantMedicine(
                    obj.getString("name"),
                    obj.getString("description"),
                    obj.getDouble("price"),
                    obj.getString("imageUrl")
                );
                medicines.add(medicine);
            }
        } catch (NullPointerException e) {
            throw new IOException("File not found: " + filePath);
        }

        return medicines;
    }
}

