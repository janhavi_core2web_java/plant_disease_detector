package com.plant.Shop;

public class PlantMedicine {

    private String name;
    private String description;
    private double price;
    private String imageUrl;
    private int rating;

    public PlantMedicine(String name, String description, double price, String imageUrl) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.imageUrl = imageUrl;
    }

    public String getName() { return name; }
    public String getDescription() { return description; }
    public double getPrice() { return price; }
    public String getImageUrl() { return imageUrl; }

    @Override
    public String toString() {
        return name;
    }

    public int getRating() {
       return rating;
    }
    
    public void setRating(int rating) {
        if (rating < 0 || rating > 5) {
            throw new IllegalArgumentException("Rating must be between 0 and 5.");
        }
        this.rating = rating;
    }
}


