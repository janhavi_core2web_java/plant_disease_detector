package com.plant.Shop;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ShopApp extends VBox {

    private TilePane medicineTilePane;
    private List<PlantMedicine> cart = new ArrayList<>();


    private TextField searchBar;

    public ShopApp(){
        initializeUI();
        initialize();
    }


    private HBox createHeader() {
        Label titleLabel = new Label("PLANT MEDICINE SHOP");
        titleLabel.setFont(Font.font("Arial", FontWeight.BOLD, 28));
        titleLabel.setTextFill(Color.DARKGREEN);
    
        
        HBox header = new HBox(20, titleLabel);
        header.setAlignment(Pos.CENTER);
        header.setPadding(new Insets(20));
        header.setStyle("-fx-background-color:#ffe4cd ; -fx-border-color: #E0E0E0; -fx-border-width: 0 0 1px 0;");
        return header;
    }
    

    public void initializeUI(){
       

        HBox header = createHeader();

        searchBar = new TextField();
        searchBar.setPromptText("Search medicines...");
        searchBar.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                filterMedicines(newValue);
            } 
            catch (IOException e) { 
                e.printStackTrace();
            }
        });
        
        // Other existing UI code...
        
        VBox searchBox = new VBox(10, searchBar);
        searchBox.setPadding(new Insets(10));

        medicineTilePane = new TilePane();
        medicineTilePane.setPadding(new Insets(10));
        medicineTilePane.setHgap(30);
        medicineTilePane.setVgap(30);
        medicineTilePane.setAlignment(Pos.CENTER);

        
        ScrollPane scrollPane = new ScrollPane(medicineTilePane);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
   
        


       // HBox hb = new HBox(medicineTilePane);

        Button viewCartButton = new Button("View Cart");
        viewCartButton.setOnAction(event -> handleViewCart());
        viewCartButton.setStyle(
            "-fx-font-size: 14px; " +
            "-fx-background-color: RED; " + // Green background
            "-fx-text-fill: white; " + // White text
            "-fx-background-radius: 5px; " + // Rounded corners
            "-fx-padding: 10 20;" + // Padding
            "-fx-border-color: transparent;" + // Transparent border
            "-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.4), 10, 0.5, 0, 0);" // Drop shadow effect
        );
    

        


        Button checkoutButton = new Button("Checkout");
        checkoutButton.setOnAction(event -> handleCheckout(null));
        checkoutButton.setStyle(
            "-fx-font-size: 14px; " +
            "-fx-background-color:RED; " +
            "-fx-text-fill: white; " + // White text
            "-fx-background-radius: 5px; " + // Rounded corners
            "-fx-padding: 10 20;" + // Padding
            "-fx-border-color: transparent;" + // Transparent border
            "-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.4), 10, 0.5, 0, 0);" // Drop shadow effect
        );

          

        //VBox sideButtons = new VBox(10, viewCartButton, checkoutButton);
        // sideButtons.setPadding(new Insets(80));
        VBox buttonsBox = new VBox(10, viewCartButton, checkoutButton);
        buttonsBox.setAlignment(Pos.CENTER_RIGHT);
        buttonsBox.setPadding(new Insets(10));
    
    

        BorderPane root = new BorderPane();
        root.setTop(header);
        root.setCenter(scrollPane);
        root.setRight(buttonsBox);
        root.setLeft(searchBox);
        root.setPadding(new Insets(20));
        root.setStyle("-fx-background-color:#5b3256;"); // Light gray background
        getChildren().add(root);
    }
    
    private void filterMedicines(String query) throws IOException {
    ObservableList<PlantMedicine> filteredList = FXCollections.observableArrayList();
    for (PlantMedicine medicine : DataFetcher.fetchData("data.json")){
            if (medicine.getName().toLowerCase().contains(query.toLowerCase())) {
                filteredList.add(medicine);
            }
        }
        displayMedicines(filteredList);
    }
    
    private void initialize() {
        try {
            List<PlantMedicine> medicines = DataFetcher.fetchData("data.json");
            ObservableList<PlantMedicine> observableMedicines = FXCollections.observableArrayList(medicines);
            displayMedicines(observableMedicines);
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    private void displayMedicines(ObservableList<PlantMedicine> medicines) {
   
        medicineTilePane.getChildren().clear();

        for (PlantMedicine medicine : medicines)
        {
     
            VBox medicineBox = new VBox(25);
            medicineBox.setPadding(new Insets(15));
            medicineBox.setSpacing(15);
            medicineBox.setStyle("-fx-border-color: #E0E0E0; -fx-border-width: 1px; -fx-background-color: #FFFFFF; -fx-background-radius: 10px; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.1), 10, 0.5, 0, 0);");
            medicineBox.setAlignment(Pos.CENTER);
            medicineBox.setOnMouseEntered(event -> {
            medicineBox.setStyle("-fx-border-color: Green; -fx-border-width: 3px; -fx-background-color: LightBlue; -fx-background-radius: 10px; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.1), 10, 0.5, 0, 0);");
            });
            medicineBox.setOnMouseExited(event -> {
            medicineBox.setStyle("-fx-border-color: #E0E0E0; -fx-border-width: 3px; -fx-background-color: #FFFFFF; -fx-background-radius: 10px; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.1), 10, 0.5, 0, 0);");
            });
        

            ImageView imageView = new ImageView(new Image(medicine.getImageUrl()));
            imageView.setFitWidth(300);
            imageView.setFitHeight(300);
            imageView.setPreserveRatio(true); 
            imageView.setStyle("-fx-border-color: #ddd; -fx-border-width: 1px; -fx-border-radius: 5px;");

            Label nameLabel = new Label(medicine.getName());
            nameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 18));
            nameLabel.setTextFill(Color.web("#333"));

            Text desc = new Text(medicine.getDescription());
            desc.setFont(Font.font("Arial", FontWeight.BOLD, 18));
            desc.setFill(Color.web("#333"));
            desc.setWrappingWidth(200);

            Label priceLabel = new Label(String.format("₹%.2f", medicine.getPrice()));
            priceLabel.setFont(Font.font("Arial", FontWeight.BOLD, 18));
            priceLabel.setTextFill(Color.web("#333"));
            
            Label ratingLabel = new Label("Rating: " + medicine.getRating() + " / 5");
            ratingLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
            ratingLabel.setTextFill(Color.web("#333"));
            
            HBox quantityBox = new HBox(5);
            Label quantityLabel = new Label("Qty:");
            quantityLabel.setTextFill(Color.DARKBLUE);
            quantityLabel.setFont(new Font(22));
            Spinner<Integer> quantitySpinner = new Spinner<>(1, 99, 1);
            quantityBox.getChildren().addAll(quantityLabel, quantitySpinner);
            quantityBox.setAlignment(Pos.CENTER);

            Button addToCartButton = new Button("Add to Cart");
            addToCartButton.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
            addToCartButton.setStyle("-fx-background-color: #4CAF50; -fx-text-fill: white; -fx-background-radius: 5px;");
            addToCartButton.setOnAction(event -> handleAddToCart(medicine));

            // Detailed View Button
            Button detailedViewButton = new Button("View Details");
            detailedViewButton.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
            detailedViewButton.setStyle("-fx-background-color: #03A9F4; -fx-text-fill: white; -fx-background-radius: 5px;");
            detailedViewButton.setOnAction(event -> handleViewDetails(medicine));

            // Buttons HBox
            HBox buttonsBox = new HBox(10, addToCartButton, detailedViewButton);
            buttonsBox.setAlignment(Pos.CENTER);

            HBox ratingBox = new HBox(5);
            ratingBox.setAlignment(Pos.CENTER);
            for (int i = 1; i <= 5; i++) {
                Button starButton = new Button("☆");
                final int rating = i;
                starButton.setOnAction(event -> {
                    medicine.setRating(rating);
                    starButton.setStyle("-fx-background-color:yellow");

                    ratingLabel.setText("Rating: " + medicine.getRating() + " / 5");
                });
                ratingBox.getChildren().add(starButton);
            }

            medicineBox.getChildren().addAll(imageView,nameLabel,priceLabel,ratingLabel,ratingBox,quantityBox, buttonsBox);
            medicineTilePane.getChildren().add(medicineBox);
            medicineTilePane.setPadding(new Insets(120));
        }
    }

    // Method to handle viewing details of a medicine
    private void handleViewDetails(PlantMedicine medicine) {
        // Create a new Stage for the detailed view
        Stage stage = new Stage();
        stage.setTitle(medicine.getName() + " Details");

        // Create a VBox layout for the detailed view
        VBox layout = new VBox(10);
        layout.setPadding(new Insets(20));
        layout.setAlignment(Pos.CENTER);

        // Medicine image
        ImageView imageView = new ImageView(new Image(medicine.getImageUrl()));
        imageView.setFitWidth(200);
        imageView.setFitHeight(200);
        imageView.setPreserveRatio(true);

        // Medicine name
        Label nameLabel = new Label(medicine.getName());
        nameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        nameLabel.setTextFill(Color.web("#333"));

        // Medicine description
        Text desc = new Text(medicine.getDescription());
        desc.setFont(Font.font("Arial", FontWeight.NORMAL, 14));
        desc.setFill(Color.web("#666"));
        desc.setWrappingWidth(300);

        // Medicine price
        Label priceLabel = new Label(String.format("₹%.2f", medicine.getPrice()));
        priceLabel.setFont(Font.font("Arial", FontWeight.BOLD, 18));
        priceLabel.setTextFill(Color.web("#333"));

        // Close button
        Button closeButton = new Button("Close");
        closeButton.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
        closeButton.setStyle("-fx-background-color: #4CAF50; -fx-text-fill: white; -fx-background-radius: 5px;");
        closeButton.setOnAction(event -> stage.close());

        

        // Add all components to the layout
        layout.getChildren().addAll(imageView, nameLabel, desc, priceLabel, closeButton);

        // Create a new Scene and set it to the stage
        Scene scene = new Scene(layout, 400, 500);
        stage.setScene(scene);
        stage.show();
    }

    private void handleAddToCart(PlantMedicine medicine)
    {
        cart.add(medicine);
    }

    // CartItem class to store medicine and quantity
    private void handleViewCart() {
       
        Stage cartStage = new Stage();
        cartStage.setTitle("Shopping Cart");
        
        // Create a VBox to hold cart items
        VBox cartItemsBox = new VBox(10);
        cartItemsBox.setPadding(new javafx.geometry.Insets(10));
        cartItemsBox.setStyle("-fx-background-color:Peachpuff");
        
        // Loop through cart items and create a styled box for each item
        for (PlantMedicine item : cart) {
            javafx.scene.layout.BorderPane pane = new javafx.scene.layout.BorderPane();
            pane.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-padding: 10px;");
        
            // Display item details
            javafx.scene.control.Label itemName = new javafx.scene.control.Label(item.getName());
            javafx.scene.control.Label itemPrice = new javafx.scene.control.Label("₹" + item.getPrice());
            javafx.scene.image.ImageView itemImage = new javafx.scene.image.ImageView(new Image(item.getImageUrl()));
            itemImage.setFitWidth(150);
            itemImage.setPreserveRatio(true);
            
        
            pane.setLeft(itemImage);
            VBox vbox = new VBox(itemName, itemPrice);
            vbox.setAlignment(Pos.CENTER);
            pane.setCenter(vbox);
        
            cartItemsBox.getChildren().add(pane);
        }


        ScrollPane scrollPane = new ScrollPane(cartItemsBox);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        
        
        // Optional: Display total price
        double totalPrice = cart.stream().mapToDouble(PlantMedicine::getPrice).sum();
        javafx.scene.control.Label totalPriceLabel = new javafx.scene.control.Label("Total Price: ₹" + totalPrice);
        
        // Create a button to close the cart window
        javafx.scene.control.Button closeButton = new javafx.scene.control.Button("Close");
        closeButton.setOnAction(event -> cartStage.close());
        

        // Create a VBox to hold all components
        VBox cartLayout = new VBox(10,scrollPane, cartItemsBox, totalPriceLabel, closeButton);
        cartLayout.setPadding(new Insets(10));
        
        javafx.scene.Scene cartScene = new javafx.scene.Scene(cartLayout, 400, 400);
        cartStage.setScene(cartScene);
        
        // Block interaction with other windows until this one is closed
        cartStage.initModality(Modality.APPLICATION_MODAL);
        cartStage.show();
    }
        
    
    
    private void handleCheckout(Scene alert) {
        // Calculate total price
        double totalPrice = cart.stream().mapToDouble(PlantMedicine::getPrice).sum();

    
    
        // Display confirmation dialo
        String message = String.format("Total Price: ₹%.2f\n\nProceed with checkout?", totalPrice);
        javafx.scene.control.Alert alert2 = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.CONFIRMATION);
        alert2.setTitle("Checkout Confirmation");
        alert2.setHeaderText(null);
        alert2.setContentText(message);
   
    
        // Handle user's response
        java.util.Optional<javafx.scene.control.ButtonType> result = alert2.showAndWait();

        double p  = 0.00;

        if(totalPrice == p && result.get() == javafx.scene.control.ButtonType.OK){
            javafx.scene.control.Alert successAlert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.WARNING);
            successAlert.setTitle("Checkout Successful");
            successAlert.setHeaderText(null);
            successAlert.setContentText("no items added!");
            successAlert.showAndWait();
        }

        if(totalPrice != p && result.get() == javafx.scene.control.ButtonType.OK){
            javafx.scene.control.Alert successAlert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.INFORMATION);
            successAlert.setTitle("Checkout Successful");
            successAlert.setHeaderText(null);
            successAlert.setContentText("ThankYou for Purchase !");
            successAlert.showAndWait();
        
        }
    }
        
}



    

    
   



