package com.plant.Dashboard;

import com.google.cloud.firestore.DocumentSnapshot;
import com.plant.controller.LoginController;
import com.plant.firebaseConfig.DataService;
import com.plant.mainWindow.TabPaneDemo;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class UserPage {
        
    static String userName;

    private DataService dataService;

    VBox vb;

    public UserPage(DataService dataService){

        this.dataService = dataService;
    }

    public Parent createUserScene(Runnable logoutHandler){


        Button logoutButton = new Button("Logout");
        logoutButton.setStyle("-fx-background-color: #000000; -fx-text-fill: white; -fx-font-size: 21px; -fx-padding: 10px 20px;-fx-background-radius: 40;");
        

        Label dataLabel = new Label();

        try{

            String key = LoginController.key;

            System.out.println("Value of key:" + key);

            DocumentSnapshot dataObject = dataService.getData("users",key);

            userName = dataObject.getString("username");

            System.out.println("username fetched"+ userName);

            dataLabel.setText(userName);


        }
        catch(Exception ex){

            ex.printStackTrace();
        }

        logoutButton.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event){
                logoutHandler.run();
            }
        });

        TabPaneDemo tabPaneDemo = new TabPaneDemo();
        Parent tabPane = tabPaneDemo.getTabPane();
    
        vb = new VBox(10,tabPane,logoutButton);
        vb.setStyle("-fx-background-color:#d8d7d7; -fx-padding: 10;");
        vb.setAlignment(Pos.CENTER);

        return vb;


    }
}
