package com.plant.mainWindow;

import com.plant.Shop.ShopApp;
import com.plant.tabWindow.Tab1Window;
import com.plant.tabWindow.Tab3Window;

import javafx.geometry.Pos;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;

public class TabPaneDemo extends HBox{
    
    private TabPane tabPane;

    public TabPaneDemo() {
        tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        
        Tab tab1 = new Tab("Disease");
        Tab1Window tb1 = new Tab1Window();
        tab1.setContent(tb1);
        tb1.setAlignment(Pos.TOP_LEFT);


        Tab tab2 = new Tab("Shop");
        ShopApp tb2 = new ShopApp();
        tab2.setContent(tb2);
        tb2.setAlignment(Pos.TOP_LEFT);

        Tab tab3 = new Tab("About Us");
        Tab3Window tb3 = new Tab3Window();
        tab3.setContent(tb3);

        //Tab tab4 = new Tab("Contact Us");

        tabPane.getTabs().addAll(tab1, tab2,tab3);
    }
   
    public TabPane getTabPane()
    {
        return tabPane;
    } 
    

}
