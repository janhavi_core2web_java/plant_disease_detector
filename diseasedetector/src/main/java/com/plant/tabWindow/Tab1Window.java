package com.plant.tabWindow;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import java.io.IOException;
import com.plant.service.APIService;

public class Tab1Window extends HBox {

    private VBox apiBox;

    public Tab1Window() {
        setSpacing(0);
        setAlignment(Pos.CENTER);

        apiBox = new VBox();
        apiBox.setAlignment(Pos.CENTER_LEFT);
        apiBox.setMaxSize(800, 800);
        apiBox.setSpacing(10);
        apiBox.setPadding(new Insets(20));
        apiBox.setLayoutX(400);

        ScrollPane scrollPane = new ScrollPane(apiBox);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        VBox buttonBox = initializeComponents1();
        buttonBox.setAlignment(Pos.CENTER_LEFT);
        //buttonBox.setMinWidth(300);

        ScrollPane buttonScrollPane = new ScrollPane(buttonBox);
        buttonScrollPane.setFitToHeight(true);
        buttonScrollPane.setFitToWidth(true);
        buttonScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        buttonScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        // buttonScrollPane.setStyle("-fx-background: transparent; -fx-background-color: #ffdab9 ;");
        // buttonBox.setStyle("-fx-background-color: transparent;");

        scrollPane.setStyle("-fx-background: transparent; -fx-background-color: #aaf0d1 ;");
        apiBox.setStyle("-fx-background-color: transparent;");
        
        getChildren().addAll(buttonBox, buttonScrollPane,scrollPane);
    }

    private VBox initializeComponents1() {
        APIService apiService = new APIService();
        try {
            apiService.displayDisease(apiBox, 5);
        } catch (IOException e) {
            e.printStackTrace();
        }
        addSolutionButton(apiBox, 5, apiService);
        

        Image img1 = new Image(getClass().getResource("/powderyMildew.jpeg").toExternalForm());
        ImageView iv1 = new ImageView(img1);
        iv1.setPreserveRatio(true);
        iv1.setFitHeight(200);
        iv1.setFitWidth(200);
        Button b1 = createButton("Powdery Mildew", 5, apiService);
        

        Image img2 = new Image(getClass().getResource("/rust.jpeg").toExternalForm());
        ImageView iv2 = new ImageView(img2);
        iv2.setPreserveRatio(true);
        iv2.setFitHeight(200);
        iv2.setFitWidth(200);
        Button b2 = createButton("Rust", 6, apiService);

        Image img3 = new Image(getClass().getResource("/leafScorch.jpeg").toExternalForm());
        ImageView iv3 = new ImageView(img3);
        iv3.setPreserveRatio(true);
        iv3.setFitHeight(200);
        iv3.setFitWidth(200);
        Button b3 = createButton("Leaf Scorch", 7, apiService);

        Image img4 = new Image(getClass().getResource("/crown.jpeg").toExternalForm());
        ImageView iv4 = new ImageView(img4);
        iv4.setPreserveRatio(true);
        iv4.setFitHeight(200);
        iv4.setFitWidth(200);
        Button b4 = createButton("Crown Gall", 12, apiService);

        Image img5 = new Image(getClass().getResource("/fairyRing.jpeg").toExternalForm());
        ImageView iv5 = new ImageView(img5);
        iv5.setPreserveRatio(true);
        iv5.setFitHeight(200);
        iv5.setFitWidth(200);
        Button b5 = createButton("Fairy Ring", 1, apiService);



        Image img6 = new Image(getClass().getResource("/fungi.jpeg").toExternalForm());
        ImageView iv6 = new ImageView(img6);
        iv6.setPreserveRatio(true);
        iv6.setFitHeight(200);
        iv6.setFitWidth(200);
        Button b6 = createButton("Fungi", 2, apiService);
 

        Image img7 = new Image(getClass().getResource("/brownrot.jpeg").toExternalForm());
        ImageView iv7 = new ImageView(img7);
        iv7.setPreserveRatio(true);
        iv7.setFitHeight(200);
        iv7.setFitWidth(200);
        Button b7 = createButton("Brown Rot", 10, apiService);

        Image img8 = new Image(getClass().getResource("/mushroom.jpeg").toExternalForm());
        ImageView iv8 = new ImageView(img8);
        iv8.setPreserveRatio(true);
        iv8.setFitHeight(200);
        iv8.setFitWidth(200);
        Button b8 = createButton("Mushrooms", 4, apiService);

        VBox vb1 = new VBox(10, iv1, b1, iv2, b2, iv3, b3, iv4, b4,iv5,b5,iv6,b6,iv7,b7,iv8,b8);
        vb1.setAlignment(Pos.CENTER_LEFT);
        vb1.setMaxSize(500, 1000);
        vb1.setStyle("-fx-background-color:#ffdab9");
        vb1.setPadding(new Insets(20));

        ScrollPane sc = new ScrollPane(vb1);
        sc.setFitToHeight(true);
        sc.setFitToWidth(true);
        sc.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sc.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sc.setPrefViewportHeight(400);

        
        return vb1;
    }

    private Button createButton(String text, int diseaseId, APIService apiService) {
        Button button = new Button(text);
        button.setMaxSize(300, 400);
        button.setTextFill(Color.WHITE);
        button.setFont(new Font(20));
        button.setStyle("-fx-background-color:darkblue");
        button.setOnAction(event -> {
            try {
                apiService.displayDisease(apiBox, diseaseId);
                addSolutionButton(apiBox, diseaseId, apiService);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return button;
    }

    private void addSolutionButton(VBox apiBox, int diseaseId, APIService apiService) {
        Button solutionButton = new Button("Solution");
        solutionButton.setMaxSize(300, 400);
        solutionButton.setTextFill(Color.WHITE);
        solutionButton.setFont(new Font(20));
        solutionButton.setStyle("-fx-background-color:green");
        solutionButton.setOnAction(event -> {
            try {
                apiService.displaySolution(apiBox, diseaseId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        apiBox.getChildren().add(solutionButton);
    }
}
