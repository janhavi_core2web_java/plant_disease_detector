package com.plant.tabWindow;

import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Tab3Window extends VBox{

    public Tab3Window() {

        Text title = new Text("About Us");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 40));
        title.setFill(Color.WHITE);


        
        Text description = new Text(
            "Welcome to Plant Care,your go-to app for all plant-related information. Our mission is to help you grow healthy plants by providing comprehensive information on various crops and their diseases.Our Plant Information application is designed to provide comprehensive information about various crops and their diseases. Our goal is to assist farmers and gardeners in identifying and managing plant diseases effectively with our user-friendly interface and detailed plant information, users can make informed decisions to ensure the health and productivity of their crops. This application provides information on crop diseases and crops. It is designed to help users identify and manage plant diseases effectively.");
        description.setWrappingWidth(1800);
        description.setFont(new Font(25));

        
        Text feature = new Text("Features : View information about common crop diseases.\n\t- Learn about different types of crops.\n\t- Get tips on managing plant health.");
        feature.setWrappingWidth(1800);
        feature.setFont(new Font(25));

        Text functionText = new Text("Concept used in this project : API Binding, Firebase Authentication, Inheritance, Exception Handling, Multithreading, Javafx");
        functionText.setWrappingWidth(1800);
        functionText.setFont(new Font(25));


        Text team = new Text("Our Team : Our team is composed of passionate plant enthusiasts and experts dedicated to making plant care accessible and easy.");

        team.setWrappingWidth(1800);
        team.setFont(new Font(25));


        Text members = new Text("Team Members : Janhavi Vispute, Anjali Dhage, Rachana Suralkar, Tejaswi Ithape.");
        members.setWrappingWidth(1800);
        members.setFont(new Font(25));

        Text mentors = new Text("Mentors : Shashi Sir ,Pramod Sir, Sachin Sir.");
        mentors.setWrappingWidth(1800);
        mentors.setFont(new Font(25));

        HBox hb1 = new HBox(title);
        hb1.setAlignment(Pos.CENTER);
        hb1.setStyle("-fx-background-color:#321414 ;");
        hb1.setMinHeight(300);

        VBox vb2 = new VBox(30,description,feature,functionText,team,members,mentors);
        vb2.setAlignment(Pos.CENTER_LEFT);
        

        HBox hb2 = new HBox(vb2);
        hb2.setAlignment(Pos.CENTER);
        hb2.setStyle("-fx-background-color:#ffddca;");
        hb2.setMinHeight(600);

        VBox vb = new VBox(hb1,hb2);
        vb.setAlignment(Pos.CENTER);

        ScrollPane scrollPane = new ScrollPane(vb);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        this.getChildren().addAll(scrollPane);
    }

}